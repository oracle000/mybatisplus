
package com.baomidou.springmvc.model.system;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

//import java.time.LocalDateTime;

@Data
@ToString
@TableName("sys_dept")
@EqualsAndHashCode(callSuper = true)
public class SysDept extends Model<SysDept>
{
    private static final long serialVersionUID = 1L;

    /**
     * 机构主键
     * varchar(32) [32]
     */
    @TableId
    //@ApiModelProperty(value="机构主键")
    private String deptId;
    /**
     * 机构编号
     * varchar(20) [20]
     */
    //@ApiModelProperty(value="客户编号")
    private String code;
    /**
     * 机构简称
     * varchar(50) [50]
     */
    //@ApiModelProperty(value="机构简称")
    private String shortName;
    /**
     * 机构全称
     * varchar(256) [256]
     */
    //@ApiModelProperty(value="机构全称")
    private String allName;
    /**
     * 统一社会信用证代码
     * varchar(32) [32]
     */
    //@ApiModelProperty(value="统一社会信用证代码")
    private String usciCode;
    /**
     * 证书名称(目录名)
     * varchar(64) [64]
     */
    //@ApiModelProperty(value="证书名称(目录名)")
    private String certName;
    /**
     * 排序
     * int(11) [10,0]
     */
    //@ApiModelProperty(value="排序")
    private Integer sort;
    /**
     * 创建时间
     * datetime []
     *
     */
    //@ApiModelProperty(value="创建时间")
//    private LocalDateTime createTime;
    /**
     * 修改时间
     * datetime []
     *
     */
    //@ApiModelProperty(value="修改时间")
//    private LocalDateTime updateTime;
    /**
     * 是否删除  1：已删除  0：正常
     * char(1) [1]
     */
    //@ApiModelProperty(value="是否删除  1：已删除  0：正常")
    private String delFlag;
    /**
     * 上级编号,无上级为0
     * varchar(32) [32]
     */
    //@ApiModelProperty(value="上级编号,无上级为0")
    private String parentId;
    /**
     * 所属租户
     * varchar(32) [32]
     */
    //@ApiModelProperty(value="所属租户")
    private String tenantId;
}
