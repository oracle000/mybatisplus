package com.baomidou.springmvc.controller;

import com.baomidou.springmvc.model.system.SysDept;
import com.baomidou.springmvc.service.system.SysDeptService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequiredArgsConstructor
@Slf4j
@Controller
@RequestMapping("/test")
public class TestController
{
    @Autowired
    private final SysDeptService sysDeptService;

    @RequestMapping("/writeDemo/{id}")
    @ResponseBody
    public SysDept writeDemoDemo(@PathVariable String id)
    {

        SysDept sysDept = sysDeptService.querySysDept1(id);
        log.info(sysDept.toString());
        sysDept = sysDeptService.querySysDept2(id);
        log.info(sysDept.toString());
        return sysDept;
    }

    @RequestMapping("/readDemo/{id}")
    @ResponseBody
    public SysDept readDemo(@PathVariable String id)
    {
        SysDept sysDept = sysDeptService.querySysDept2(id);
        return sysDept;
    }
}
