package com.baomidou.springmvc.datasource;

import java.lang.annotation.*;

@Target( ElementType.METHOD)
@Retention( RetentionPolicy.RUNTIME)
@Inherited
public @interface DataSource
{
   String name() default "write";
}