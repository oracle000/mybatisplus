
package com.baomidou.springmvc.service.system.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.springmvc.datasource.DataSource;
import com.baomidou.springmvc.mapper.system.SysDeptMapper;
import com.baomidou.springmvc.model.system.SysDept;
import com.baomidou.springmvc.service.system.SysDeptService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@RequiredArgsConstructor
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService
{
    @DataSource(name = "write")
    public SysDept querySysDept1(String id)
    {
        SysDept sysDept = super.getById(id);
        return sysDept;
    }

    @DataSource(name = "read")
    public SysDept querySysDept2(String id)
    {
        SysDept sysDept = super.getById(id);
        return sysDept;
    }
}

