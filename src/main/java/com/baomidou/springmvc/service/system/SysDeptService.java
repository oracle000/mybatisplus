
package com.baomidou.springmvc.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.springmvc.model.system.SysDept;


public interface SysDeptService extends IService<SysDept>
{
    public SysDept querySysDept1(String id);

    public SysDept querySysDept2(String id);
}
