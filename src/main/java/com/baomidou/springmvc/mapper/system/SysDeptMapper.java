
package com.baomidou.springmvc.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.springmvc.model.system.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 机构管理
 *
 * @author zhiqiang
 * @date 2021-10-20 09:38:45
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept>
{


}
